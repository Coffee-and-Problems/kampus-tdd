﻿using System;
using System.Collections.Generic;
using System.Linq;
using BowlingGame.Infrastructure;
using FluentAssertions;
using NUnit.Framework;

namespace BowlingGame
{
    public class Game
    {
        public int score;
        public LinkedList<Frame> frames;
        public class Frame
        {
            public int pins1;
            public int pins2;
            public int pins3;
            public bool isFirstTry;

            private bool isSpare;
            public bool IsSpare
            {
                get
                {
                    if (pins1 != 10)
                        return pins1 + pins2 == 10;
                    else
                        return false;
                }
            }

            public bool IsStrike
            {
                get => pins1 == 10; 
            }
        }

        public Game()
        {
            score = 0;
            frames = new LinkedList<Frame>();
            frames.AddLast(new Frame() {pins1 = 0, pins2 = 0, pins3 = 0, isFirstTry = true});
        }

        public void Roll(int pins)
        {

            if (frames.Count == 10)
            {
                if (frames.Last.Value.IsSpare)
                    frames.Last.Value.pins3 = pins;
            }

            if (frames.Last.Value.isFirstTry)
            {
                frames.Last.Value.pins1 += pins;
                frames.Last.Value.isFirstTry = false;
            }
            else
            {
                if (frames.Last.Value.IsStrike)
                {
                    frames.AddLast(new Frame() {pins1 = 0, pins2 = 0, pins3 = 0, isFirstTry = true});
                    return;
                }
                frames.Last.Value.pins2 += pins;
                if (frames.Count < 10) frames.AddLast(new Frame() {pins1 = 0, pins2 = 0, pins3 = 0, isFirstTry = true});
            }

        }

        public int GetScore()
        {
            var bonus1 = 1;
            var bonus2 = 1;

            foreach (var frame in frames)
            {
                score += (frame.pins1*bonus1 + frame.pins2*bonus2 + frame.pins3);

                bonus1 = frame.IsSpare ? 2 : 1;
                bonus1 = frame.IsStrike ? 2 : 1;
                bonus2 = frame.IsStrike ? 2 : 1;
            }

            return score;
        }
    }

    [TestFixture]
    public class Game_should : ReportingTest<Game_should>
    {
        internal Game game;

        [SetUp]
        public void setup()
        {
            game = new Game();
        }


        [Test]
        public void HaveZeroScore_BeforeAnyRolls()
        {
            game.GetScore()
                .Should().Be(0);
        }

        [Test]
        public void HaveOnePoint_HitOnePin()
        {
            game.Roll(1);
            game.GetScore()
                .Should().Be(1);
        }

        [Test]
        public void HavePointsSum_ForTwoRoll()
        {
            game.Roll(2);
            game.Roll(4);
            game.GetScore()
                .Should().Be(6);
        }

        [Test]
        public void Have11_TwoFrames()
        {
            game.Roll(2);
            game.Roll(7);
            game.Roll(2);

            game.GetScore()
                .Should().Be(11);
        }

        
        [Test]
        public void HaveBonusScore_AfterSpare()
        {
            game.Roll(5);
            game.Roll(5);
            game.Roll(2);

            game.GetScore()
                .Should().Be(14);
        }

        [Test]
        public void LastFrameIsSpare_AndOtherNot()
        {
            for (int i = 0; i < 18; i++)
            {
                game.Roll(1);
            }

            game.Roll(5);
            game.Roll(5);
            game.Roll(2);

            game.GetScore()
                .Should().Be(32);
        }

        [Test]
        public void ThrowException_WhenTry11Frame()
        {
            for (int i = 0; i < 20; i++)
                game.Roll(1);

            Assert.Throws<ArgumentOutOfRangeException>(() => game.Roll(1));
        }

        [Test]
        public void CantRollAfterLastBonus()
        {
            for (int i = 0; i < 18; i++)
            {
                game.Roll(1);
            }

            game.Roll(5);
            game.Roll(5);
            game.Roll(2);
            
            Assert.Throws<ArgumentOutOfRangeException>(() => game.Roll(1));
        }

        [Test]
        public void OneStrike()
        {
            game.Roll(10);
            game.Roll(2);
            game.Roll(2);

            game.GetScore()
                .Should().Be(18);
        }
    }
}
